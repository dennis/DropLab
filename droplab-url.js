/**
 * @name
 * droplab-url
 * 
 * @description
 * Adds ability to load external data as custom options.
 *
 * To use, simply add [data-url="endpoint"] to your droplab trigger. The URL can be relative or absolute, with
 * the data being an array of objects.
 *
 * @example
  <div class="dropdown">
    <a href='#' data-url="data.json" data-dropdown-trigger='#dropdown-3' class='btn'>I have external data!</a>
    <div class='dropdown-menu' id='dropdown-3' data-dropdown>
      <ul>
        <li><a href="#">About</a></li>
        <li><a href="#">Home</a></li>
      </ul>
      <ul data-dynamic>
        <li><a href='#' data-id='{{id}}'>{{text}}</a></li>
      </ul>
    </div>
  </div>
 * 
 * TODO: Add tests!
 */
(function(w){
  var dl = w.DropLab;
  var pluginName = 'url';

  var initialize = function() {
    var successHandler = function(hook, data) {
      hook.list.setData(data);
    }
    var errorHandler = function() {
      throw error(pluginName, ': Something went wrong, stopping initialization of plugin.');
    }

    dl.prototype.hooks.forEach(function(hook) {
      if(hook.trigger.dataset.hasOwnProperty('url')) {
        fetchData(hook, successHandler, errorHandler);
      }
    });

    return true;
  }

  var fetchData = function(hook, successHandler, errorHandler) {
    var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    var url = hook.trigger.dataset.url;

    xhr.open('get', url, true);
    xhr.onreadystatechange = function() {
      var statusCode;
      var data;

      if (xhr.readyState === 4) {
        statusCode = xhr.status;

        if (statusCode === 200) {
          data = JSON.parse(xhr.responseText);
          successHandler && successHandler(hook, data);
        } else {
          console.error(pluginName, ': Error fetching external data.');
          errorHandler && errorHandler();
        }
      }
    }

    xhr.send();
  }

  dl.extend({
    name: pluginName,
    initialize: initialize
  });

}(window));

